import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 * @author Pratheek
 */

public class Snake {

    public static void main(String[] args) {
        new Snake();
    }

    /* GUI stuff */
    private JPanel board;
    private JButton[] snakeBodyPart;
    private JButton bonusfood;
    private JTextArea ScoreBoard;

    private final int SNAKE_RUNNING_SPEED_FASTEST = 25;
    private final int SNAKE_RUNNING_SPEED_FASTER = 50;
    private final int SNAKE_RUNNING_SPEED_FAST = 100;
    private final int BOARD_WIDTH = 700;
    private final int BOARD_HEIGHT = 450;
    private final int SCORE_BOARD_HEIGHT = 80;
    private final int SNAKE_LENGTH_DEFAULT = 4;
    private final int SNAKE_BODY_PART_SQUARE = 10;
    private final int BONUS_FOOD_SQUARE = 15;
    private final Point INIT_POINT = new Point(100, 150);

    private enum GAME_TYPE {NO_MAZE, BORDER, TUNNEL};
    private int selectedSpeed = SNAKE_RUNNING_SPEED_FASTER;
    private GAME_TYPE selectedGameType = GAME_TYPE.NO_MAZE;
    private int totalBodyPart;
    private int dirX;
    private int dirY;
    private int score;
    private Point pointOfBonusFood = new Point();
    private boolean isGoingLeft;
    private boolean isGoingRight;
    private boolean isGoingUp;
    private boolean isGoingDown;
    private boolean isBonusFoodAvail;
    private boolean isRunning;
    private Random rand = new Random();

    Snake() {
        resetToDefaults();
        initGUI();
        createInitSnake();
        isRunning = true;
        startThread();
    }

    public void initGUI() {
        JFrame frame = new JFrame("Snake");
        frame.setSize(500, 330);
        setJMenuBar(frame);
        JPanel scorePanel = new JPanel();
        ScoreBoard = new JTextArea("Score: " + score);
        ScoreBoard.setEnabled(false);
        ScoreBoard.setBackground(Color.BLUE);
        board = new JPanel();
        board.setLayout(null);
        board.setBounds(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
        board.setBackground(Color.WHITE);
        scorePanel.setLayout(new GridLayout(0, 1));
        scorePanel.setBounds(0, BOARD_HEIGHT, BOARD_WIDTH, SCORE_BOARD_HEIGHT);
        scorePanel.setBackground(Color.RED);
        scorePanel.add(ScoreBoard);

        frame.getContentPane().setLayout(null);
        frame.getContentPane().add(board);
        frame.getContentPane().add(scorePanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyPress(keyEvent e) {
//                snakeKeyPress(e);
//            }
//        });
        frame.setResizable(false);
    }

    public void setJMenuBar(JFrame frame) {
        JMenuBar mybar = new JMenuBar();
        JMenu game = new JMenu("Game");
        JMenuItem newgame = new JMenuItem("New Game");
        JMenuItem exit = new JMenuItem("Exit!");
        newgame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startNewGame();
            }
        });
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        game.add(newgame);
        game.addSeparator();
        game.add(exit);
        mybar.add(game);

        JMenu type = new JMenu("Type");
        JMenuItem noMaze = new JMenuItem("No Maze");
        noMaze.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedGameType = GAME_TYPE.NO_MAZE;
                startNewGame();
            }
        });
        JMenuItem border = new JMenuItem("Border Maze");
        border.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedGameType = GAME_TYPE.BORDER;
                startNewGame();
            }
        });
        type.add(noMaze);
        type.add(border);
        mybar.add(type);

        JMenu level = new JMenu("Level");
        JMenuItem level1 = new JMenuItem("Level 1");
        level1.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                selectedSpeed = SNAKE_RUNNING_SPEED_FAST;
                startNewGame();
            }
        });
        JMenuItem level2 = new JMenuItem("Level 2");
        level2.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                selectedSpeed = SNAKE_RUNNING_SPEED_FASTER;
                startNewGame();
            }
        });
        JMenuItem level3 = new JMenuItem("Level 3");
        level3.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                selectedSpeed = SNAKE_RUNNING_SPEED_FASTEST;
                startNewGame();
            }
        });
        level.add(level1);
        level.add(level2);
        level.add(level3);
        mybar.add(level);
        frame.setJMenuBar(mybar);
    }

    public void resetToDefaults() {
        snakeBodyPart = new JButton[2000];
        totalBodyPart = SNAKE_LENGTH_DEFAULT;
        dirX = SNAKE_BODY_PART_SQUARE;
        dirY = 0;
        score = 0;
        isGoingLeft = false;
        isGoingRight = true;
        isGoingUp = true;
        isGoingDown = true;
        isBonusFoodAvail = false;
    }

    void startNewGame() {
        resetToDefaults();
        board.removeAll();
        createInitSnake();
        ScoreBoard.setText("Score: " + score);
        isRunning = true;
    }

    /*
     * This method is responsible to init snake with 4 body parts
     *
     */
    public void createInitSnake() {
        int x = (int) INIT_POINT.getX();
        int y = (int) INIT_POINT.getY();
        for (int i = 0; i < totalBodyPart; i++) {
            snakeBodyPart[i] = new JButton();
            snakeBodyPart[i].setBounds(x, y, SNAKE_BODY_PART_SQUARE, SNAKE_BODY_PART_SQUARE);
            snakeBodyPart[i].setBackground(Color.GRAY);
            board.add(snakeBodyPart[i]);
            x = x - SNAKE_BODY_PART_SQUARE;
        }
        createFood();
    }

    void createFood() {
        int randX = SNAKE_BODY_PART_SQUARE + (SNAKE_BODY_PART_SQUARE * rand.nextInt(48));
        int randY = SNAKE_BODY_PART_SQUARE + (SNAKE_BODY_PART_SQUARE * rand.nextInt(23));
        snakeBodyPart[totalBodyPart] = new JButton();
        snakeBodyPart[totalBodyPart].setEnabled(false);
        snakeBodyPart[totalBodyPart].setBounds(randX, randY, SNAKE_BODY_PART_SQUARE, SNAKE_BODY_PART_SQUARE);
        board.add(snakeBodyPart[totalBodyPart]);
        totalBodyPart++;
    }

    void createBonusFood() {
        bonusfood = new JButton();
        bonusfood.setEnabled(false);
        int bonusFoodLocX = SNAKE_BODY_PART_SQUARE * rand.nextInt(50);
        int bonusFoodLocY = SNAKE_BODY_PART_SQUARE * rand.nextInt(25);
        bonusfood.setBounds(bonusFoodLocX, bonusFoodLocY, BONUS_FOOD_SQUARE, BONUS_FOOD_SQUARE);
        pointOfBonusFood = bonusfood.getLocation();
        board.add(bonusfood);
        isBonusFoodAvail = true;
    }

    void processNextStep() {
        boolean isBorderTouched = false;
        int newHeadLocX = (int)snakeBodyPart[0].getLocation().getX();
        int newHeadLocY = (int)snakeBodyPart[0].getLocation().getY();
        int foodLocX = (int)snakeBodyPart[totalBodyPart - 1].getLocation().getX();
        int foodLocY = (int)snakeBodyPart[totalBodyPart - 1].getLocation().getY();

        if (newHeadLocX >= BOARD_WIDTH - SNAKE_BODY_PART_SQUARE) {
            newHeadLocX = 0;
            isBorderTouched = true;
        } else if (newHeadLocX <= 0) {
            newHeadLocX = BOARD_WIDTH - SNAKE_BODY_PART_SQUARE;
            isBorderTouched = true;
        } else if (newHeadLocY >= BOARD_HEIGHT - SNAKE_BODY_PART_SQUARE) {
            newHeadLocY = 0;
            isBorderTouched = true;
        } else if (newHeadLocY <= 0) {
            newHeadLocY = BOARD_HEIGHT - SNAKE_BODY_PART_SQUARE;
            isBorderTouched = true;
        }

        if (newHeadLocX == foodLocX && newHeadLocY == foodLocY) {
            score += 5;
            ScoreBoard.setText("Score: " + score);
            if (score % 50 == 0 && !isBonusFoodAvail) {
                createBonusFood();
            }
            createFood();
        }
        if (isBonusFoodAvail &&
                pointOfBonusFood.x <= newHeadLocX &&
                pointOfBonusFood.y <= newHeadLocY &&
                (pointOfBonusFood.x + SNAKE_BODY_PART_SQUARE) >= newHeadLocX &&
                (pointOfBonusFood.y + SNAKE_BODY_PART_SQUARE) >= newHeadLocY) {
            board.remove(bonusfood);
            score += 100;
            ScoreBoard.setText("Score: " + score);
            isBonusFoodAvail = false;
        }
        if (isGameOver(isBorderTouched, newHeadLocX, newHeadLocY)) {
            ScoreBoard.setText("Score: " + score);
            isRunning = false;
            return;
        } else {
            moveSnakeForward(newHeadLocX, newHeadLocY);
        }
        board.repaint();
    }

    private boolean isGameOver(boolean isBorderTouched, int headLocX, int headLocY) {
        switch(selectedGameType) {
            case BORDER:
                if(isBorderTouched) {
                    return true;
                }
                break;
            default:
                break;
        }

        for (int i=SNAKE_LENGTH_DEFAULT; i<totalBodyPart - 2; i++) {
            Point partLoc = snakeBodyPart[i].getLocation();
            System.out.println("(" + partLoc.x + ", " + partLoc.y + ") (" + headLocX + ", " + headLocY + ")");
            if (partLoc.equals(new Point(headLocX, headLocY))) {
                return true;
            }
        }
        return false;
    }

    public void moveSnakeForward(int headLocX, int headLocY) {
        for (int i=totalBodyPart - 2; i>0; i--) {
            Point frontBodyPartPoint = snakeBodyPart[i-1].getLocation();
            snakeBodyPart[i].setLocation(frontBodyPartPoint);
        }
        snakeBodyPart[0].setBounds(headLocX, headLocY, SNAKE_BODY_PART_SQUARE, SNAKE_BODY_PART_SQUARE);
    }

    public void snakeKeyPress(KeyEvent e) {
        if (isGoingLeft == true && e.getKeyCode() == 37) {
            dirX = -SNAKE_BODY_PART_SQUARE;
            dirY = 0;
            isGoingRight = false;
            isGoingUp = true;
            isGoingDown = true;
        }

        if (isGoingUp == true && e.getKeyCode() == 38) {
            dirX = 0;
            dirY = -SNAKE_BODY_PART_SQUARE;
            isGoingDown = false;
            isGoingRight = true;
            isGoingLeft = true;
        }

        if (isGoingRight == true && e.getKeyCode() == 39) {
            dirX = +SNAKE_BODY_PART_SQUARE;
            dirY = 0;
            isGoingLeft = false;
            isGoingUp = true;
            isGoingDown = true;
        }

        if (isGoingDown == true && e.getKeyCode() == 40) {
            dirX = 0;
            dirY = +SNAKE_BODY_PART_SQUARE;
            isGoingUp = false;
            isGoingRight = true;
            isGoingLeft = true;
        }
    }

    private void startThread() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                runIt();
            }
        });
        thread.start();
    }

    public void runIt() {
        while(true) {
            if(isRunning) {
                processNextStep();
                try {
                    Thread.sleep(selectedSpeed);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }

}
