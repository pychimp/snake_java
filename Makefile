JCC = javac
JFLAGS = -verbose

Snake:
	$(info --- Building: $@ ---)
	-$(JCC) $(JFLAGS) $@.java 2> error.log

clean:
	$(info --- Running: Clean ---)
	rm -rf *.class
	rm -rf *.log
